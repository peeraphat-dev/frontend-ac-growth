import React from "react";
import Layout from '../../components/admin/admin.layout/AdLayout';
import AdHome from "../../components/admin/admin.product/AdHome";

export default function adhome() {
  return (
    <Layout>
      <AdHome />
    </Layout>
  );
}
