import React from "react";
import Link from 'next/link';
import { Form, Input, Button, Checkbox, Row, Col, Card } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

export default function login() {
  return (
    <Row className='bg-admin'>
      <Col xs={{ span: 22,offset:1 }} sm={{span: 14,offset:5}} md={{span:12,offset:6}} lg={{span:8,offset:8}}>
        <Card title="Adminstrator">
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{ remember: true }}
          >
            <Form.Item
              name="username"
              rules={[
                { required: true, message: "Please input your Username!" },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
              />
            </Form.Item>

            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Login
              </Button>
            {' '}Or  <Link href="/" ><a>A.C. Growth Steel</a></Link>
            </Form.Item>
          </Form>
        </Card>
      </Col>
    </Row>
  );
}
