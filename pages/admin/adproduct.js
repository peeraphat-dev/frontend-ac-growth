import React from "react";
import Layout from '../../components/admin/admin.layout/AdLayout';
import AdProduct from "../../components/admin/admin.product/AdProduct";

export default function adproduct({type}) {
  return (
    <Layout>
      <AdProduct type={type.data}/>
    </Layout>
  );
}


export async function getStaticProps(context) {
  const url = 'https://my-mongodb-api-delta.vercel.app/api/types'
  const res = await fetch(url)
  const data = await res.json()
  return {
    props: {
      type: data
    }, // will be passed to the page component as props
  }
}