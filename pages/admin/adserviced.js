import React from "react";
import Layout from '../../components/admin/admin.layout/AdLayout';
import AdServiced from "../../components/admin/admin.product/AdServiced";


export default function adserviced() {
  return (
    <Layout>
      <AdServiced />
    </Layout>
  );
}
