import React from "react";
import Layout from '../../components/admin/admin.layout/AdLayout';
import Content from '../../components/admin/admin.product/Content'

export default function index() {
  return (
    <Layout>
      <Content />
    </Layout>
  );
}
