import React from "react";
import Layout from "../components/layout/Layout";
import { Container, Row, Col } from "reactstrap";
import FormContact from "../components/pageContact/FormContact";
import MapAcgrowth from "../components/pageContact/MapAcgrowth";

export default function contact() {
  return (
    <Layout>
      <Container fluid className='bg-mail' >
      <Container >
        <Row style={{padding:'100px 0'}}>
        <Col md={5} lg={4}>
            <div className="scale-page-contact">
              <ul style={{listStyle:'none'}}><h4>ติดต่อสั่งซื้อสินค้า</h4>
                <li >บริษัท A.C. Growth Steel จำกัด </li>
                <li >99 หมู่ที่ 9 ตำบล lorem อำเภอ lorem จังหวัด lorem 99999</li>
                <li style={{color:'blue'}}>โทร. +66 666 6666</li>
                <li style={{color:'red'}}>จันทร์ – ศุกร์ : 8:00 AM to 5:00 PM</li>
              </ul>
              <FormContact />
            </div>
          </Col>
          <Col >
            <div >
              <MapAcgrowth />
            </div>
          </Col>
        </Row>
      </Container>
      </Container>
    </Layout>
  );
}
