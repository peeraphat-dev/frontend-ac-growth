import React from "react";
import Layout from "../components/layout/Layout";
import { Container, Row, Col } from "reactstrap";

export default function about() {
  return (
    <Layout>
        <Container fluid className='bg-about-page'>
        <Container>
        <Row>
          <Col>
            <div className="scale-page-about ">
              <h1 style={{color:'#ffffff'}}>เกี่ยวกับ</h1>
              <p style={{color:'#ffffee', fontSize:'16px'}}>
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam
                rerum quae quia nihil quas? Accusamus dolor sint, harum
                veritatis assumenda illum eos alias magnam repellendus ea
                eveniet, quidem et tempora?
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam
                rerum quae quia nihil quas? Accusamus dolor sint, harum
                veritatis assumenda illum eos alias magnam repellendus ea
                eveniet, quidem et tempora?
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam
                rerum quae quia nihil quas? Accusamus dolor sint, harum
                veritatis assumenda illum eos alias magnam repellendus ea
                eveniet, quidem et tempora?
              </p>
              </div>
          </Col>
        </Row>
      </Container>
      </Container>
    </Layout>
  );
}
