import React,{useState} from "react";
import Layout from "../components/layout/Layout";
import { Container } from "reactstrap";
import TapPage from "../components/pageProduct/TapPage";

export default function product(props) {


  
  return (
    <Layout >
      <Container fluid className='bg-serv'>
      <Container >
        <div className="scale-page-sub ">
      <TapPage apiData={props.apiData} apiService={props.apiService}/>
        </div>
      </Container>
      </Container>
    </Layout>
  );
}

export async function getStaticProps(context) {
  const url = 'https://my-mongodb-api-delta.vercel.app/products'
  const res = await fetch(url)
  const data = await res.json()

  // const url1 = 'http://localhost:3000/api/services/items'
  // const res1 = await fetch(url1)
  // const data1 = await res1.json()

  return {
    props: {
      apiData: data,
      apiService: data.service.filter(v=> v.isUsed === true),
    }, 
  }
}
