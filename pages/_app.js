import React from 'react'
import '../styles/style.css'
import "bootstrap/dist/css/bootstrap.min.css";
import 'antd/dist/antd.css';
import {StoreProvider} from "../store/storeProvider"
export default function App({ Component, pageProps }) {
    return (
        <StoreProvider>
            <Component {...pageProps} />
        </StoreProvider>
    )
}
