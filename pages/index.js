import React, { useState } from "react"
import Layout from "../components/layout/Layout"
import CarouselPage from "../components/pageHome/Carousel"
import ShowCard from "../components/pageHome/ShowCard"
import AboutUs from "../components/pageHome/AboutUs"
import ProductUs from "../components/pageHome/ProductUs"

export default function index(props) {
	return (
		<Layout>
			<CarouselPage />
			<ProductUs apiData={props.apiData} />
			<AboutUs />
			<ShowCard apiService={props.apiService} />
		</Layout>
	)
}

export async function getStaticProps(context) {
	const url = "https://my-mongodb-api-delta.vercel.app/products"
	const res = await fetch(url)
	const data = await res.json()

	return {
		props: {
			apiData: data,
			apiService: data.service.filter(v => v.isUsed === true),
		},
	}
}
