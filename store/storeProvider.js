import React,{ createContext, useState} from 'react'
export const StoreContext = createContext()

export const  StoreProvider=({children}) =>{
    const initialState = { 
        page : "1"
    }
    const [state, setstate] = useState(initialState)

    const setPage = (value)=>{
        setstate({
            page : value
        })
    }   

    const {page} = state
    return (
        <StoreContext.Provider value={{page, setPage}}>
            {children}
        </StoreContext.Provider>
    )
}
