import React, { useEffect, useState, useContext } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
} from "reactstrap";
import classnames from "classnames";
import CardProduct from "./CardProduct";
import AllService from "./AllService";
import { StoreContext } from "../../store/storeProvider";
const TapPage = (props) => {



  const [activeTab, setActiveTab] = useState("1");

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  useEffect(() => {
    setActiveTab(page)
  }, []);

  const { page } = useContext(StoreContext);

  return (
    <div>
      <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === "1" })}
            style={{color:'#ffffff'}}
            onClick={() => {
              toggle("1");
            }}
          >
            Product
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === "2" })}
            style={{color:'#ffffff'}}
            onClick={() => {
              toggle("2");
            }}
          >
            Service
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1">
          <Row>
            <Col sm="12">
              <CardProduct apiData={props.apiData} />
            </Col>
          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
            <Col sm="12">
              <AllService apiService={props.apiService}/>
            </Col>
          </Row>
        </TabPane>
      </TabContent>
    </div>
  );
};

export default TapPage;
