import React from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Container,
  Row,
  Col,
} from "reactstrap";

export default function AllService(props) {
  return (
    <Container style={{ padding: "0 0" }}>
      <h1
        style={{ color: "#ffffff", marginBottom: "10px", paddingTop: "15px" }}
      >
        Service
      </h1>
      <Row>
        <Col md={4} style={{ margin: "10px 0" }}>
          <Card>
            <CardImg
              top
              width="100%"
              src="/images/img-01.jpg"
              alt="Card image cap"
            />
          </Card>
        </Col>
        <Col style={{ margin: "10px 0" }}>
          <h4 style={{ color: "#ffffff" }}>{props.apiService[0].headerService}</h4>
          <p style={{ color: "#ffffff" }}>
          {props.apiService[0].description}
          </p>
        </Col>
      </Row>

      <Row></Row>
    </Container>
  );
}
