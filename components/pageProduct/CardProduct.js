import React, { useState, useEffect } from "react";
import alasql from "alasql";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
  Container,
  Row,
  Col,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  ListGroup,
  ListGroupItem,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

export default function ProductUs(props) {
  // console.log(props.apiData.type);

  const [dropdownOpen, setOpen] = useState(false);
  const dropToggle = () => setOpen(!dropdownOpen);

  const [modal, setModal] = useState(false);
  const [header, setHeader] = useState(false);
  const [description, setDescription] = useState(false);
  const [newData, setNewData] = useState([]);
  const [count, setCount] = useState(0);
  const [num, setNum] = useState(1);
  const [a, setA] = useState(0);
  const [b, setB] = useState(0);
  const [menuName, setMenuName] = useState("All Product");

  const setData = (page, type) => {
    const data = !type
      ? props.apiData.data
      : props.apiData.data.filter((v) => v.typeProduct === type);
    if (page === num) return;

    let pagPost = null;
    let offset = 0;
    const fatch = 9;
    if (!page || page === 1) {
      pagPost = alasql(
        `SELECT * FROM ? OFFSET ${offset} ROWS FETCH NEXT ${fatch} ROWS ONLY`,
        [data]
      );
    } else {
      offset = 9 * page - 9;
      pagPost = alasql(
        `SELECT * FROM ? OFFSET ${offset} ROWS FETCH NEXT ${fatch} ROWS ONLY`,
        [data]
      );
    }
    setNewData(pagPost);
    setNum(page ? page : 1);
    setA(page ? page : 1);
  };

  const PagRender = () => {
    let list = [];
    for (let i = 0; i < count; i++) {
      list.push(
        <PaginationItem active={num === i + 1 ? true : false} key={i}>
          <PaginationLink
            onClick={() => {
              setData(i + 1);
            }}
          >
            {i + 1}
          </PaginationLink>
        </PaginationItem>
      );
    }
    return list;
  };
 
  useEffect(() => {
    if (props.apiData) {
      let num = 0;
      setCount(props.apiData.data.length / 9);
      setData();
      for (let index = 0; index < count; index++) {
        setB(index + 1);
      }
    }
  }, []);

  const toggle = (v, a) => {
    setModal(!modal);
    setHeader(v);
    setDescription(a);
  };
  const close = () => setModal(!modal);

  return (
    <div>
      <div style={{ marginTop: "20px" }}>
        <Row>
          <Col md={3} lg={2} className="menu-product">
            <h2 style={{ color: "#ffffff" }}>Menu</h2>

            <ListGroup className="menuswither">
              <ListGroupItem
                onClick={() => {
                  setData(undefined, undefined);
                  setMenuName("All Product");
                }}
              >
                All Product
              </ListGroupItem>
              {props.apiData.type.map((value, index) => {
                return (
                  <ListGroupItem
                    onClick={() => {
                      setData(undefined, value.typeName);
                      setMenuName(value.typeName);
                    }}
                    key={index}
                  >
                    {value.typeName}
                  </ListGroupItem>
                );
              })}
            </ListGroup>

            <ButtonDropdown
              className="menuswith btn-menu"
              isOpen={dropdownOpen}
              toggle={dropToggle}
            >
              <DropdownToggle caret>{menuName}</DropdownToggle>
              <DropdownMenu>
              <DropdownItem
                      onClick={() => {
                        setData(undefined, undefined);
                        setMenuName("All Product");
                      }}
                    >
                      All Product
                    </DropdownItem>
                {props.apiData.type.map((value, index) => {
                  return (
                    <DropdownItem
                    onClick={() => {
                      setData(undefined, value.typeName);
                      setMenuName(value.typeName);
                    }}
                      key={index}
                    >
                      {value.typeName}
                    </DropdownItem>
                  );
                })}
              </DropdownMenu>
            </ButtonDropdown>
          </Col>
          <Col md={9} lg={10}>
            <h1 style={{ color: "#ffffff" }}>{menuName}</h1>

            <div className="card-cuttom-product">
              <Container>
                <Row>
                  {newData.map((value, index) => {
                    return (
                      <Col
                        sm={6}
                        md={6}
                        lg={4}
                        style={{ margin: "10px 0" }}
                        key={index}
                      >
                        <Card
                          onClick={() =>
                            toggle(value.productName, value.description)
                          }
                        >
                          <CardImg
                            top
                            width="100%"
                            src="/images/img-01.jpg"
                            alt="Card image cap"
                          />
                          <CardBody>
                            <CardTitle>
                              <h4 style={{ color: "#ffffff" , cursor:'pointer' }}>
                                {value.productName}
                              </h4>{" "}
                            </CardTitle>
                            <CardText className="blink-text">
                              {`> > Click to Detail.`}
                            </CardText>
                          </CardBody>
                        </Card>
                      </Col>
                    );
                  })}
                </Row>
                <Modal isOpen={modal} toggle={close}>
                  <ModalHeader toggle={close}>{header}</ModalHeader>
                  <ModalBody>
                    <div>
                      <img width="100%" src="images/img-01.jpg" alt="" />
                    </div>
                    <div style={{ marginTop: "5px" }}>{description}</div>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="secondary" onClick={close}>
                      Close
                    </Button>
                  </ModalFooter>
                </Modal>
                <Row>
                  <Col>
                    <Pagination aria-label="Page navigation example ">
                      <PaginationItem
                        disabled={num === 1}
                        onClick={() => {
                          setData();
                        }}
                      >
                        <PaginationLink first />
                      </PaginationItem>
                      <PaginationItem disabled={num === 1}>
                        <PaginationLink
                          onClick={() => {
                            setData(a - 1);
                          }}
                          next
                          previous
                        />
                      </PaginationItem>
                      <PagRender />

                      <PaginationItem disabled={num === b}>
                        <PaginationLink
                          onClick={() => {
                            setData(a + 1);
                          }}
                          next
                        />
                      </PaginationItem>
                      <PaginationItem disabled={num === b}>
                        <PaginationLink
                          onClick={() => {
                            setData(b);
                          }}
                          last
                        />
                      </PaginationItem>
                    </Pagination>
                  </Col>
                </Row>
              </Container>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
