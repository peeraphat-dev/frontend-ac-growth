import React,{useState} from 'react'
import Link from 'next/link';
import { IoMdMenu } from "react-icons/io";
import {
    Collapse,
    Navbar,
    Nav,
    NavItem,
    Button,
    NavLink
  } from 'reactstrap';

export default function Header() {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <div className='header-position'>
      <Navbar className='bg-header' light expand="md">
      <h4 className='logo-brand'><Link href="/" ><a>A.C. Growth Steel</a></Link></h4>
      <Button className="burger-button"  onClick={toggle} ><IoMdMenu className="burger-icon"/></Button>
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
            <Link href="/"><NavLink href="/">หน้าหลัก</NavLink></Link>
            </NavItem>
            <NavItem>
            <Link href="/product"><NavLink href="/product">สินค้าบริการ</NavLink></Link>
            </NavItem>
            <NavItem>
            <Link href="/about"><NavLink href="/about">เกี่ยวกับ</NavLink></Link>
            </NavItem>
            <NavItem>
            <Link href="/contact"><NavLink href="/contact">ติดต่อ</NavLink></Link>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
     </div>
  
    )
}
