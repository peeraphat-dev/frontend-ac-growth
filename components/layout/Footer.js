import React from "react";
import Link from "next/link";
import { Container, Row, Col } from "reactstrap";
import {
  FacebookOutlined,
  HomeOutlined,
  PhoneOutlined,
} from "@ant-design/icons";

export default function Footer() {
  return (
    <div className="footer">
      <div className="bt-footer">
        <Container>
          <Row>
            
            <Col md={4} style={{ margin: "10px 0" }}>
              <div>
                <h4 className="bt-text d-flex">
                  <PhoneOutlined className="ic-footer" />{" "}
                  <div className='pl-1'> +66 666 6666</div>
                </h4>
                <h4 className="bt-text d-flex">
                  <FacebookOutlined /><div className='pl-1'>Lorem@facebook.com</div> 
                </h4>
              </div>
            </Col>
            <Col md={8} style={{ margin: "10px 0" }}>
              <div style={{display:'flex', justifyContent:'center'}}>
                <h4 className="bt-text bt-contact">
                  contact
                </h4>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      <p>Copyright 2020 © บริษัท A.C. Growth Steel</p>
    </div>
  );
}
