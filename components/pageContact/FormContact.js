import React from "react";
import { Col, Row, Button, Form, FormGroup, Input } from "reactstrap";

export default function FormContact() {
  return (
    <div style={{marginTop:'20px'}}>
    <Form>
      <Row >
        <Col md={6}>
          <FormGroup>
            {/* <Label>Name</Label> */}
            <Input
              type="email"
              name="email"
              id="exampleEmail"
              placeholder="ชื่อ-นามสกุล"
            />
          </FormGroup>
        </Col>
        <Col md={6}>
        <FormGroup>
            {/* <Label>Email</Label> */}
            <Input
              type="email"
              name="email"
              id="exampleEmail"
              placeholder="email"
            />
          </FormGroup>
        </Col>
      </Row>
      <FormGroup>
        {/* <Label for="exampleText">Text Area</Label> */}
        <Input style={{height:'200px'}} type="textarea" name="text" id="exampleText" placeholder="ข้อความ"/>
      </FormGroup>
      <Button style={{width:'100%'}}>ส่ง</Button>
    </Form>
    </div>
  );
}
