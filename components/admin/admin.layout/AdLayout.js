import React, { useState, useEffect } from "react";
import Link from "next/link";
import route from "next/router";
import { Layout, Menu, Button, Modal } from "antd";
import {
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";

const { Header, Content, Footer, Sider } = Layout;

export default function testSide(props) {
  const [current, setCurrent] = useState([]);
  const handleClick = (e) => {
    setCurrent(e.key);
  };

  useEffect(() => {
    const path = route.route.split("/")[2];
    setCurrent(path);
  }, []);


  const [visible, setVisible] = useState(false)

  const showModal = () => {
    setVisible(true);
  };

 const handleOk = e => {
    setVisible(false);
    route.push('/admin/login')
  };

  const handleCancel = e => {

    setVisible(false);
  };

  return (
    <Layout>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
      >
        <div>
          <h5 style={{ color: "#ffffff", padding: "10px 0 0 10px" }}>
            ADMINSTRATOR
          </h5>
          <h6 style={{ color: "rgb(167,199,208)", padding: "0 0 0 10px" }}>
            A.C. Growth Steel
          </h6>
        </div>
        <hr className="admin-hr" />
        <Menu
          theme="dark"
          mode="inline"
          onClick={handleClick}
          selectedKeys={[current]}
        >
          <Menu.Item key="adhome" icon={<UserOutlined />}>
            <Link href="/admin/adhome">• Page-Home</Link>
          </Menu.Item>
          <Menu.Item key="adproduct" icon={<UserOutlined />}>
            <Link href="/admin/adproduct">• Page-Product</Link>
          </Menu.Item>
          <Menu.Item key="adserviced" icon={<UserOutlined />}>
            <Link href="/admin/adserviced">• Page-Service</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header
          className="site-layout-sub-header-background"
          style={{ padding: 0, display:'flex', alignItems:'center' }}
        ><Button onClick={showModal} style={{marginLeft:'auto', marginRight:'10px'}} type="primary" danger>LOGOUT</Button>
        <Modal
          title="Logout"
          visible={visible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <p>คุณต้องการออกจากระบบ ?</p>
        </Modal>
        </Header>
        <Content style={{ margin: "24px 16px 0" }}>
          <div
            className="site-layout-background"
            style={{ padding: 24, minHeight: 360 }}
          >
            {props.children}
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Copyright © 2020 A.C. Growth Steel. All rights reserved.
        </Footer>
      </Layout>
    </Layout>
  );
}
