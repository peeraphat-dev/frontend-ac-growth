import React from "react";
import {
  AvForm,
  AvField,
  AvInput,
  AvGroup,
  AvFeedback,
} from "availity-reactstrap-validation";
import {
  Button,
  Label,
  Container,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

export default function AdServiced() {
  return (
    <Container>
      <h1>Page-Service</h1>
      <hr />
      <AvForm>
        <AvGroup>
          <Label for="serviceName">Service Name </Label>
          <AvInput name="serviceName" id="serviceName" required />
          <AvFeedback>กรุณากรอกชื่อบริการ!</AvFeedback>
        </AvGroup>

        <AvGroup>
          <Label for="serviceDetail">Service Detail</Label>
          <AvInput
            type="textarea"
            name="serviceDetail"
            id="serviceDetail"
            required
          />
          <AvFeedback>กรุณากรอกรายละเอียดบริการ!</AvFeedback>
        </AvGroup>

        <AvGroup>
          <Label for="serviceImage">Service Image</Label>
          <AvInput
            type="file"
            name="serviceImage"
            id="serviceImage"
            required
          />
        <AvFeedback>กรุณาใส่รูปภาพตัวอย่างบริการ!</AvFeedback>
        </AvGroup>
        <AvGroup>
        <Button color="primary">Confirm</Button>
        </AvGroup>
      </AvForm>
    </Container>
  );
}
