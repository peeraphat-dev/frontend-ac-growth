import React from 'react'

export default function Content() {
    return (
        <div>
           <h4>Adminstrator คือระบบที่ใช้สำหรับจัดการแก้ไข เพิ่ม ลบ ข้อมูลภายในหน้าเว็บเพื่อให้ง่ายต่อการจัดการหน้าเว็บด้วยตนเอง.</h4> 
           <h5 style={{color:'red'}}>***ห้ามบุคคลภายนอกรู้ Username และ Password ของ Admin เด็ดขาดเพื่อป้องกันข้อมูลภายในเว็บเสียหาย</h5>
        </div>
    )
}
