import React, { useState, useEffect } from "react";
import {
  AvForm,
  AvField,
  AvInput,
  AvGroup,
  AvFeedback,
} from "availity-reactstrap-validation";
import {
  Label,
  Container,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { Table, Button } from "antd";

const columns = [
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Age",
    dataIndex: "age",
  },
  {
    title: "Address",
    dataIndex: "address",
  },
];

const data = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    name: `Edward King ${i}`,
    age: 32,
    address: `London, Park Lane no. ${i}`,
  });
}

export default function AdHome() {
  // const [state, setstate] = useState(initialState)
  // const {} = state
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loading, setLoading] = useState(false);

  const start = () => {
    setLoading(true);
    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
    }, 1000);
  };

  const onSelectChange = (selectedRowKeys) => {
    setSelectedRowKeys(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const hasSelected =
    selectedRowKeys.length !== 0 && selectedRowKeys.length > 0;

  return (
    <Container>
      <h1>Page-Home</h1>
      <hr />
      {/* คอลัสเซล */}
      <AvForm>
      
        <Label>
            <h3>Select Show-Caousel</h3>
          </Label>
        
        <AvGroup>
          <Label >showcaousel at 1</Label>
          <AvInput
            type="file"
            name="showcaouselone"
            id="showcaouselone"
            required
          />
        <AvFeedback>กรุณาใส่รูปภาพที่จะแสดง!</AvFeedback>
        </AvGroup>
        <AvGroup>
          <Label >showcaousel at 2</Label>
          <AvInput
            type="file"
            name="showcaouseltwo"
            id="showcaouseltwo"
            required
          />
        <AvFeedback>กรุณาใส่รูปภาพที่จะแสดง!</AvFeedback>
        </AvGroup>
        <AvGroup>
          <Label >showcaousel at 3</Label>
          <AvInput
            type="file"
            name="showcaouselthree"
            id="showcaouselthree"
            required
          />
        <AvFeedback>กรุณาใส่รูปภาพที่จะแสดง!</AvFeedback>
        </AvGroup>
        <Button type="primary" color="primary">Confirm</Button>
        
        <hr />
      {/* แสดงสินค้า 6 อย่าง */}
        <AvGroup>
          <Label for="showproduct">
            <h3>Select Show-Product</h3>
          </Label>
          <div>
            <div style={{ marginBottom: 16 }}>
              <Button
                type="primary"
                onClick={start}
                disabled={!hasSelected}
                loading={loading}
              >
                Reload
              </Button>
              <span style={{ marginLeft: 8 }}>
                {hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}
              </span>
            </div>
            <Table
              rowSelection={rowSelection}
              columns={columns}
              dataSource={data}
            />
          </div>
          <Button type="primary" color="primary">Confirm</Button>
        </AvGroup>

        <hr />
        {/* แสดงเซอวิส 1 อย่าง */}
        <AvGroup>
          <Label for="showservices">
            <h3>Select Show-Service</h3>
          </Label>
          <div>
            <div style={{ marginBottom: 16 }}>
              <Button
                type="primary"
                onClick={start}
                disabled={!hasSelected}
                loading={loading}
              >
                Reload
              </Button>
              <span style={{ marginLeft: 8 }}>
                {hasSelected ? `Selected ${selectedRowKeys.length} items` : ""}
              </span>
            </div>
            <Table
              rowSelection={rowSelection}
              columns={columns}
              dataSource={data}
            />
          </div>
          <Button type="primary" color="primary">Confirm</Button>
        </AvGroup>
      </AvForm>
    </Container>
  );
}
