import React, { useRef } from "react";
import {
  AvForm,
  AvField,
  AvInput,
  AvGroup,
  AvFeedback,
} from "availity-reactstrap-validation";
import {
  Button,
  Label,
  Container,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";

export default function AdProduct({ type }) {
  let dataSave = {
    productName: "",
    description: "",
    typeProduct: "",
    price: 0,
    isUsed: true,
  };
  let form = useRef(null);
  const save = async (e, errors, values) => {
 
    if (errors.length !== 0) return;

    dataSave.productName = values.productName;
    dataSave.description = values.productDetail;
    dataSave.typeProduct = values.select;
    dataSave.price = values.productPrice;
    const url = "http://localhost:3000/api/products";
    try {
      const res = await fetch(url, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-type": "application/json",
        },
        body: JSON.stringify(dataSave),
      });
      // form.reset();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Container>
      <h1>Page-Product</h1>
      <hr />
      <AvForm onSubmit={save} ref={(el) => (form = el)}>
        <AvGroup>
          <Label for="productName">Product Name</Label>
          <AvInput name="productName" id="productName" required />
          <AvFeedback>กรุณากรอกชื่อสินค้า!</AvFeedback>
        </AvGroup>
        <AvGroup>
          <Label for="productDetail">Product Detail</Label>
          <AvInput
            type="textarea"
            name="productDetail"
            id="productDetail"
            required
          />
          <AvFeedback>กรุณากรอกรายละเอียดสินค้า!</AvFeedback>
        </AvGroup>
        <AvGroup>
          <Label for="productPrice">Product Price</Label>
          <InputGroup>
            <AvInput
              type="number"
              name="productPrice"
              id="productPrice"
              required
            />
            <InputGroupAddon addonType="append">
              <InputGroupText>บาท</InputGroupText>
            </InputGroupAddon>
            <AvFeedback>กรุณากรอกรายราคาสินค้า!</AvFeedback>
          </InputGroup>
        </AvGroup>
        <AvGroup>
          <AvField type="select" name="select" label="Product Type">
            {type &&
              type.map((value, index) => {
                return (
                  <option value={value.typeName} key={index}>
                    {value.typeName}
                  </option>
                );
              })}
          </AvField>
        </AvGroup>
        <Button color="primary">Confirm</Button>
      </AvForm>
    </Container>
  );
}
