import React, { useState, useContext } from "react";
import Link from "next/link";
import { StoreContext } from "../../store/storeProvider";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Container,
  Row,
  Col,
} from "reactstrap";

const ShowCard = (props) => {
  const { setPage } = useContext(StoreContext);
  return (
    <div className=" bt-card card-cuttom-showcard bg-serv">
      <Container>
        <h1 style={{ color: "#ffffff", marginBottom: "10px" }}>การบริการ</h1>
        <Row>
          <Col md={4} style={{ margin: "10px 0" }}>
            <Card>
              <CardImg
                top
                width="100%"
                src="/images/img-01.jpg"
                alt="Card image cap"
              />
            </Card>
          </Col>
          <Col style={{ margin: "10px 0", color: "#ffffff" }}>
            <h4 style={{ color: "#ffffff" }}>
              {props.apiService[0].headerService}
            </h4>
            <p>{props.apiService[0].description}</p>
          </Col>
        </Row>

        <Row>
          <Col style={{ textAlign: "right" }}>
            <Link href="/product">
              <a onClick={() => setPage("2")}>ดูบริการทั้งหมด..</a>
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ShowCard;
