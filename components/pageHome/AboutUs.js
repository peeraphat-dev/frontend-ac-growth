import React, { useRef, useState } from "react";
import { Parallax } from "react-parallax";
// import { Container, Row, Col } from "reactstrap";
import Link from "next/link";
const inlineStyle = {
  background: "#fff",
  left: "50%",
  top: "50%",
  position: "absolute",
  padding: "20px",
  borderRadius: 20,
  transform: "translate(-50%, -50%)",
};

export default function AboutUs() {
  return (
    <div>
      <Parallax bgImage="images/bg-abo.jpg" strength={500}>
        <div style={{height:'150px', display:'flex', alignItems:'center', justifyContent: "center"}}>
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Link href="/about">
              <a className="head-aboutus">
                <div style={{ color: "#ffffff", fontSize: "24px" }}>About</div>
              </a>
            </Link>
          </div>
        </div>
      </Parallax>
    </div>
  );
}
