import React, { useState, useEffect, useContext } from "react";
import Link from "next/link";
import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  Container,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Pagination,
  PaginationItem,
  PaginationLink,
  ListGroup,
  ListGroupItem,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { StoreContext } from "../../store/storeProvider";

export default function ProductUs(props) {

  const [data, setData] = useState([])
  const [modal, setModal] = useState(false);

  const toggle = () => {
    setModal(!modal);
  };
  const close = () => setModal(!modal);

  const { setPage } = useContext(StoreContext);

  useEffect(() => {
    setData(props.apiData.data.filter(v=>v.isUsed === true))
  }, []);

  return (
    <div className="bt-card bg-prod">
      <Container>
        <h1 style={{ color: "#ffffff", marginBottom: "10px" }}>สินค้าขายดี</h1>
        <Row>
          {data.map((value, index) => {
            return (
              <Col md={4} style={{ margin: "10px 0" }} key={index}>
                <Card onClick={toggle}>
                  <CardImg
                    top
                    width="100%"
                    src="/images/img-01.jpg"
                    alt="Card image cap"
                  />
                  <CardBody>
                    <CardTitle>สินค้า 1</CardTitle>
                  </CardBody>
                </Card>
              </Col>
            );
          })}
        </Row>

        <Modal isOpen={modal} toggle={close}>
          <ModalHeader toggle={close}>header</ModalHeader>
          <ModalBody>
            <div>
              <img width="100%" src="images/img-01.jpg" alt="" />
            </div>
            <div style={{ marginTop: "5px" }}>description</div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={close}>
              Close
            </Button>
          </ModalFooter>
        </Modal>

        <Row>
          <Col style={{ textAlign: "right" }}>
            <Link href="/product">
              <a onClick={() => setPage("1")}>ดูสินค้าทั้งหมด..</a>
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
